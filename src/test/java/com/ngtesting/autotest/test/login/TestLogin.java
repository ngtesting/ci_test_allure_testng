package com.ngtesting.autotest.test.login;

import io.qameta.allure.Allure;
import io.qameta.allure.AllureLifecycle;
import org.testng.*;
import org.testng.annotations.*;
import org.testng.log4testng.Logger;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

@Test(testName = "登录测试")
public class TestLogin implements ITest {
    private static final Logger log = Logger.getLogger(TestLogin.class);
    AllureLifecycle lifecycle = Allure.getLifecycle();

    protected String myTestName = "";

    @Test(priority=1, testName = "登录成功") // A bug that testName annotation not work for Allure
    public void loginSuccess() {
        lifecycle.updateTestCase(testResult -> testResult.setTestCaseId("1"));
        lifecycle.updateTestCase(testResult -> testResult.setName("成功登录"));

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        String title = "homepage";
        Assert.assertEquals(title , "homepage");
    }

    @Test(priority=2, dataProvider="userAccount")
    public void loginFail(String caseId, String testName, String username, String password, String expectedTitle) {
        lifecycle.updateTestCase(testResult -> testResult.setTestCaseId(caseId));
        lifecycle.updateTestCase(testResult -> testResult.setName(testName));
        lifecycle.updateTestCase(testResult -> testResult.setDescription("login failed with username: "+username));

        String actualTitle = "登录";
        Assert.assertEquals(actualTitle , expectedTitle);
    }

    @DataProvider(name="userAccount")
    public Object[][] getUserAccountData() {
        return new Object[][] {
                {"2", "密码错误", "admin", "pass", "登录" },
                {"3", "账号过期", "tester", "pass", "登录"},
        };
    }

    @Override
    public String getTestName() {
        String name = testName.get();
        return name;
    }

    @BeforeMethod(alwaysRun = true)
    public void customTestName(Method method, Object[] testData, ITestContext ctx) {
        if (testData.length > 0) {
            this.myTestName = testData[1].toString();
        } else {
            Annotation[] anno = method.getDeclaredAnnotationsByType(org.testng.annotations.Test.class);
            if (anno.length > 0) {
                this.myTestName = ((org.testng.annotations.Test)anno[0]).testName();
            } else {
                this.myTestName = method.getName();
            }
        }

        testName.set(this.myTestName);
    }

    private ThreadLocal<String> testName = new ThreadLocal<>();
}
